var list = require('./gestorPersonatges');
var config = require("./config");

var numPregunta = 0;
var name = "";
var power = 0;
var type = "";
var existingCharacter = false;
var respondido = "";

printWelcomeMessage();
interactiveConsole();

function input(question, callback) {
  var stdin = process.stdin, stdout = process.stdout;
  stdin.resume();
  stdout.write(question);
  stdin.once('data', function(data) {
    data = data.toString().trim();
    callback(data);
  });
}

function interactiveConsole() {
  
  input( "\n>> ", function(data) {
    var parts=data.trim().split(' ');
    if (parts[0]) parts[0]=parts[0].toUpperCase();

    switch (true) {
        case (parts[0]=="Q"):
            process.exit(0);
            break;

        case (parts[0]=="LIST"):
            listPersonatges();
            break;

        case (parts[0]=="ADD"):
            if(parts[3]){
                add(parts[1], parts[2], parts[3]);
            }
            else{
                console.log("No has posat tota la informació.");
            }
            interactiveConsole();
            break;

        case (parts[0]=="DEL"):
            if(parts[1]){
                deleteCharacter(parts[1]);
            }
            else{
                console.log("Espeifica el nom del personatge que vols borrar.");
            }
            interactiveConsole();
            break;

        case (parts[0]=="SAVE"):
            if(parts[1]){
                list.save(parts[1]);
            }
            else{
                console.log("No has especificat on ho vols guardar.");
            }
            interactiveConsole();
            break;

        case (parts[0]=="LOAD"):
            if(parts[1]){
                list.load(parts[1]);
            }
            else{
                console.log("No has especificat d'on vols recuperar els personatges.");
            }
            interactiveConsole();
            break;

        case (parts[0]=="LOADDDBB"):
            list.loadbbdd();
            interactiveConsole();
            break;

        case (parts[0]=="SAVEDDBB"):
            list.savebbdd();
            interactiveConsole();
            break;

        default:
            console.log("Incorrect command");
            interactiveConsole();
            break;
        }
    });
}

function printWelcomeMessage() {
    console.log(`Avaialble commands:
    Q - Exits the program\n
    LIST - Llista els personatges.\n
    ADD Name Power Type - Afegeix un personatge a la llista amb els atributs especificats.\n
    DEL Name - Esborra el personatge Name de la llista.\n
    SAVE FileName - Desa a la llista FileName la llista de personatges actuals en format JSON.\n
    LOAD FileName - Recupera de l'arxiu especificat la llista de personatges.\n
    SAVEDDBB - Desa la llista a la base de dades.\n
    LOADDDBB - Recupera els personatges de la base de dades.`);
}

function add(nombre, poder, tipo){
    for(let i = 0; i < list.personatges.length; i++){
        if(list.personatges[i].name == nombre){
            console.log("Aquest personatge ja existeix");
            existingCharacter = true;
            break;
        }
    }
    if(!existingCharacter){
        var character = new list.personatge(nombre, poder, tipo);
        list.personatges.push(character);
    }
    else{
        existingCharacter = false;
    }
}

function listPersonatges(){
    if(list.personatges.length == 0){
        console.log("No hi ha cap personatge encara\n");
    }
    for(let i = 0; i < list.personatges.length; i++){
        console.log("Personatje " + (i + 1));
        console.log("Nom: " + list.personatges[i].name);
        console.log("Power: " + list.personatges[i].power);
        console.log("Type: " + list.personatges[i].type + "\n");
    }
    interactiveConsole();
}

function deleteCharacter(character){
    for(let i = 0; i < list.personatges.length; i++){
        if(list.personatges[i].name == character){
            list.personatges.splice(i,1);
            existingCharacter = true;
            break;
        }
    }
    if(!existingCharacter){
        console.log("Aquest personatge no existeix\n");
    }
    else{
        existingCharacter = false;
    }
}