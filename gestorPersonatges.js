var fs = require("fs");
const db = require("./db.js");

var personatges = exports.personatges = [];

var constructor = function personatge(name, power, type){
    this.name = name;
    this.power = power;
    this.type = type;
}

module.exports.personatge = constructor;

module.exports.save = function save(file){
    var jsonRead = "";
    fs.writeFile(file, JSON.stringify(personatges), function(err, data){
        if(err != null){
            console.log(err);
        }
    });
}

module.exports.load = function load(file){
    var character;
    if(fs.existsSync(file)){
        fs.readFile(file, function(err, data){
            var jsonRead = JSON.parse(data);
            for(let i = 0; i < jsonRead.length; i++){
                character = new constructor(jsonRead[i].name, jsonRead[i].power, jsonRead[i].type);
                personatges.push(character);
            }
        })
    }
    else{
        console.log("L'archiu especificat no existeix.");
    }
}

module.exports.loadbbdd = function loadbbdd(){
    db.query("SELECT * from characters;", [], function(err,data){
        for(let i = 0; i < data.rowCount; i++){
            character = new constructor(data.rows[i].name, data.rows[i].power, data.rows[i].type);
            personatges.push(character);
        }
    });
}

module.exports.savebbdd = function savebbdd(){
    var names = [];
    db.query("DELETE from characters;", [], function(err,data){
        if(err != null){
            console.log("Error deleting the database. " + err);
        }
        else{
            for(let i = 0; i < personatges.length; i++){
                db.query("INSERT into characters(name, power, type) VALUES ($1, $2, $3);", [personatges[i].name, personatges[i].power, 
                personatges[i].type], function(err, data){
                    if(err != null){
                        console.log("Erron inseting data. " + err);
                    }
                });
            }
        }
        
    });
}