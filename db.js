var config = require('./config.js')
const { Pool, Client } = require('pg')
exports.query = function(query, params, callback) {
    //postgres://user:password@host:port/database
    const connectionString = "postgres://" + config.db.username + ":" + config.db.password + "@" + config.db.host + ":" + config.db.port
    + "/" + config.db.database + "?ssl=true";
    const client = new Client({
        connectionString: connectionString,
    })
    client.connect((err) => {
        if (err) {
            console.error('connection error', err.stack);
            callback(err);
        }
    })
 
    client.query(query, params, (err, res) => {
        if (err) {
            console.log(err.stack)
            callback(err);
        } else {
            callback(err, res);
            client.end();
        }
    });
};